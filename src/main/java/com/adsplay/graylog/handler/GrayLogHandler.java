package com.adsplay.graylog.handler;
/*
 *   INFORMATIONAL => 6, # Informational (syslog source)
                    INFO          => 6, # Informational (Logger source)
                    NOTICE        => 5, # Notice
                    WARN          => 4, # Warning (Logger source)
                    ERROR         => 3, # Error
                    CRITICAL      => 2, # Critical (syslog source)
                    ALERT         => 1, # Alert (syslog source)
                    EMERGENCY     => 0} # Emergency (syslog source)
 */

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.graylog2.GelfMessage;
import org.graylog2.GelfSenderResult;
import org.graylog2.log.GelfAppender;

import com.adsplay.graylog.enumeration.GrayLevel;
import com.adsplay.graylog.utils.GrayUtils;

import rfx.core.util.FileUtils;
import rfx.core.util.LogUtil;

public class GrayLogHandler {

    private static final int MAX_SIZE_SLEEP = 50000;

    private static final String APPENDER_GRAYLOG2_ADD_EXTENDED_INFORMATION = "appender.graylog2.addExtendedInformation";

    private static final String APPENDER_GRAYLOG2_EXTRACT_STACKTRACE = "appender.graylog2.extractStacktrace";

    private static final String APPENDER_GRAYLOG2_FACILITY = "appender.graylog2.facility";

    private static final String APPENDER_GRAYLOG2_ORIGIN_HOST = "appender.graylog2.originHost";

    private static final String APPENDER_GRAYLOG2_GRAYLOG_PORT = "appender.graylog2.graylogPort";

    private static final String APPENDER_GRAYLOG2_ON = "appender.graylog2.on";

    private static final String APPENDER_GRAYLOG2_GRAYLOG_HOST = "appender.graylog2.graylogHost";

    private static final String CONFIG_PATH = "/" + "configs/" + "graylog.properties";

    private static final int LIMIT_SENDING_QUEUE = 3000;

    private static GelfAppender appender;

    private static Properties properties = new Properties();

    private static Timer timer = new Timer();

    private static Queue<GelfMessage> queryMessages = new ConcurrentLinkedQueue<GelfMessage>();

    private static Queue<GelfMessage> errorMessages = new ConcurrentLinkedQueue<GelfMessage>();

    private static ExecutorService executorService = Executors.newFixedThreadPool(10);
    
    static {
        init();
        startTimerTaskJobQueue();
        startTimerTaskJobError();
    }

    private static void startTimerTaskJobError() {
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                try {
                    int errorSize = errorMessages.size();
                    if (errorSize > 0){
                        System.out.println("Error size:" + errorSize);
                        GrayLogHandler.buildMessageAndSend(GrayLevel.WARN, "Error message notification", "FAILED", 81,
                                "Method: startTimerTaskJobError, errorMessage size: " + errorSize, new HashMap<>());
                    }
                   
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 500, 300000);
    }
    
    private static void startTimerTaskJobQueue() {
        TimerTask timerTask = new TimerTask() {

            @Override
            public void run() {
                try {
                    System.out.println("Queue Size:" + queryMessages.size());
                    int count = 0;
                    while (true) {
                        
                        if (count >= LIMIT_SENDING_QUEUE || queryMessages.size() == 0) {
                            break;
                        }
                        
                        GelfMessage gelfMessage = queryMessages.poll();

                        executorService.submit(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    GelfSenderResult result =  appender.getGelfSender().sendMessage(gelfMessage);
                                    if (result.getCode() != GelfSenderResult.OK.getCode()){
                                        errorMessages.offer(gelfMessage);
                                    }
                                    int millis = getSleepTime();
                                    Thread.sleep(millis);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            private int getSleepTime() {
                                int millis = 200;
                                if (queryMessages.size() >= MAX_SIZE_SLEEP){
                                    millis = 500;
                                } else if (queryMessages.size() >= MAX_SIZE_SLEEP/2){
                                    millis = 300;
                                }
                                return millis;
                            }
                        });
                        count++;
                        System.out.println("Remaining size:" + queryMessages.size());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, 200, 5000);
    }

    private GrayLogHandler() {

    }

    public static void init() {
        initAppender();
    }

    public static void initAppender() {

        try {
            DataInputStream streamConfigFile = FileUtils.readFileAsStream(CONFIG_PATH);
            properties.load(streamConfigFile);

            if (appender == null && ("true".equalsIgnoreCase(properties.getProperty(APPENDER_GRAYLOG2_ON)))) {
                appender = new GelfAppender();
                appender.setGraylogHost(properties.getProperty(APPENDER_GRAYLOG2_GRAYLOG_HOST));
                appender.setGraylogPort(Integer.valueOf(properties.getProperty(APPENDER_GRAYLOG2_GRAYLOG_PORT)));
                appender.setOriginHost(properties.getProperty(APPENDER_GRAYLOG2_ORIGIN_HOST, ""));
                appender.setFacility(properties.getProperty(APPENDER_GRAYLOG2_FACILITY));
                appender.setExtractStacktrace(Boolean.getBoolean(properties.getOrDefault(APPENDER_GRAYLOG2_EXTRACT_STACKTRACE, false).toString()));
                appender.setAddExtendedInformation(Boolean.getBoolean(properties.getOrDefault(APPENDER_GRAYLOG2_ADD_EXTENDED_INFORMATION, false).toString()));
                appender.activateOptions();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void buildMessageAndSend(GrayLevel level, String shortMessage, String fullMessage, int line, String className,
            Map<String, Object> additionalInfos) {
        buildMessageAndSend(level, shortMessage, fullMessage, line, className, additionalInfos, "");
    }

    public static void buildMessageAndSend(GrayLevel level, String shortMessage, String fullMessage, int line, String className,
            Map<String, Object> additionalInfos, String fieldTime) {
        try {
            if (appender != null) {
                GelfMessage gelfMessage = new GelfMessage();
                gelfMessage.setHost(properties.getProperty("appender.graylog2.sourcename"));
                gelfMessage.setVersion(properties.getProperty("appender.graylog2.version"));
                gelfMessage.setShortMessage(shortMessage);
                gelfMessage.setFullMessage(fullMessage);
                gelfMessage.setLine(String.valueOf(line));
                gelfMessage.setFile(className);
                gelfMessage.setLevel(GrayUtils.getLevel(level));
                if (additionalInfos != null) {
                    long loggedTime = ((Long)additionalInfos.getOrDefault(fieldTime, 0L));
                    gelfMessage.setJavaTimestamp(loggedTime == 0 ? System.currentTimeMillis() : loggedTime);
                    gelfMessage.setAdditonalFields(additionalInfos);
                }
                // Put message to queue to reduce sending log time
                queryMessages.offer(gelfMessage);
            }
        } catch (Exception e) {
            LogUtil.error(e);
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int i = 0;
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        long startTime = System.currentTimeMillis();
        while (i < 10000) {
            executorService.submit(new Runnable() {

                @Override
                public void run() {
                    GrayLogHandler.buildMessageAndSend(GrayLevel.CRITICAL, "test new graylog", "full test", 4, "Class", new HashMap<>());
                }
            });
            i++;
        }
        long duration = System.currentTimeMillis() - startTime;
        System.out.println(duration);
    }
}
