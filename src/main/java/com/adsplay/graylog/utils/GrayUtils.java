package com.adsplay.graylog.utils;

import com.adsplay.graylog.enumeration.GrayLevel;

public class GrayUtils {

    public static String getLevel(GrayLevel level) {
        if (level.equals(GrayLevel.INFO)) {
            return "6";
        } else if (level.equals(GrayLevel.CRITICAL)) {
            return "2";
        } else if (level.equals(GrayLevel.EMERGENCY)) {
            return "0";
        } else if (level.equals(GrayLevel.ERROR)) {
            return "3";
        } else if (level.equals(GrayLevel.NOTICE)) {
            return "5";
        } else if (level.equals(GrayLevel.WARN)) {
            return "4";
        }
        return "1";
    }

}
