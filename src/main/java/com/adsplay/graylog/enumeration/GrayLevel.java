package com.adsplay.graylog.enumeration;

public enum GrayLevel {
    INFO, ERROR, WARN, NOTICE, CRITICAL, ALERT, EMERGENCY
}
